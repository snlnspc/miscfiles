#!/bin/bash
# WARNING: this script will destroy data on the selected disk.
#
# After getting this working in 2017, I discovered mdaffin's much better bootstrap.
#   https://github.com/mdaffin/salt-arch/blob/master/bootstrap
# Much of mdaffin's script was repurposed for a single, private repo bootstrap
#   bash <(curl url/username/repo/raw/master/bs)

# mdaffin
set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

pacman -Sy --noconfirm pacman-contrib
curl -s "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&use_mirror_status=on" | \
    sed -e 's/^#Server/Server/' -e '/^#/d' | \
    rankmirrors -n 5 - > /etc/pacman.d/mirrorlist

distro=$(grep -h '^ID=' /etc/*-release | grep -o '[a-z].*[a-z]')
: ${distro:?"distro cannot be empty. failed to pull from /etc/os-release?"}
read -rep "Hostname: " hostname
: ${hostname:?"hostname cannot be empty"}

# read -rep "Bitbucket username: " -i "snlnspc" bbuser
# stty -echo
# read -rep "Bitbucket password: " bbpw
# stty echo
# read -rep $'\n'"Bitbucket repo: " -i "salt-private" bbrepo

# switched to mdaffin's key method / device list
# ssh-keygen -N "" -f bitbucket
# curl -X POST -u "${bbuser}:${bbpw}" "https://api.bitbucket.org/2.0/repositories/${bbuser}/${bbrepo}/deploy-keys" --data-urlencode "key=$(cat bitbucket.pub)" -d "label=${hostname}"
clear

if [ $distro == "arch" ] && [ $hostname != "vex" ]; then
  root="/mnt/root"
  salt="/mnt/etc/salt"

  devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
  printf "\n$devicelist\n"
  read -rep "Installation disk: " -i "/dev/sda" device
  clear

  exec 1> >(tee "stdout.log")
  exec 2> >(tee "stderr.log")

  timedatectl set-ntp true
  # swap_size=$(free --mebi | awk '/Mem:/ {print $2}')
  # swap_end=$(( $swap_size + 129 + 1 ))MiB

  parted --script "${device}" -- mklabel gpt \
         mkpart primary ext4 1Mib 513MiB \
         set 1 esp on \
         mkpart primary linux-swap 513MiB 1538MiB \
         mkpart primary ext4 1538MiB 100%

  # Simple globbing was not enough as on one device I needed to match /dev/mmcblk0p1
  # but not /dev/mmcblk0boot1 while being able to match /dev/sda1 on other devices.
  part_boot="$(ls ${device}* | grep -E "^${device}p?1$")"
  part_swap="$(ls ${device}* | grep -E "^${device}p?2$")"
  part_root="$(ls ${device}* | grep -E "^${device}p?3$")"

  wipefs "${part_boot}"
  wipefs "${part_swap}"
  wipefs "${part_root}"

  mkfs.vfat -F32 "${part_boot}"
  mkswap "${part_swap}"
  swapon "${part_swap}"
  mkfs.f2fs -f "${part_root}"

  mount "${part_root}" /mnt
  mkdir /mnt/efi
  mount "${part_boot}" /mnt/efi

  pacstrap /mnt base linux linux-firmware base-devel grub efibootmgr salt git openssh
  genfstab -t PARTUUID /mnt >> /mnt/etc/fstab
  echo "${hostname}" > /mnt/etc/hostname
else
  root="/root"
  salt="/etc/salt"
fi

# nabbed mdaffin's bootstrap flag. nice.
cat >${salt}/minion <<EOF
id: ${hostname}

file_client: local
file_roots:
  base:
    - /srv/salt/state

pillar_merge_lists: True
pillar_roots:
  base:
    - /srv/salt/pillar

renderer: jinja | yaml | gpg
EOF

mkdir -p ${root}/.ssh && cat >${root}/.ssh/config <<EOF
Host saltbb
    Hostname bitbucket.org
    IdentityFile /etc/salt/ssh/bitbucket
    IdentitiesOnly yes
    StrictHostKeyChecking no
EOF

mv salt-private /mnt/root/salt-private
cat >${root}/provision <<EOF
mkdir -p /srv
mv /root/salt-private /srv/salt
#git clone git@saltbb:snlnspc/salt-private.git && mv salt-private /srv/salt
salt-call --local state.apply --state-output=mixed pillar='{"bootstrap": True}'
EOF

chmod +x ${root}/provision

case $distro in
  arch)
    if [ $hostname == "vex" ]; then
        echo "${hostname}" > /etc/hostname
        pacman -Syu --noconfirm
        pacman -S salt git openssh --noconfirm
        bash /root/provision
    else
        arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --recheck
        arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
        arch-chroot /mnt bash /root/provision
        echo ok
        umount -R /mnt
    fi
    ;;
  manjaro)
    pacman -Syu --noconfirm
    curl -L https://bootstrap.saltstack.com | sudo sh -s -- -M -x python3
    bash /root/provision
    ;;
  opensuse-*)
    zypper -n in salt-minion git openssh
    bash /root/provision
    ;;
  debian)
    wget -O - https://repo.saltstack.com/apt/debian/9/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
    echo "deb http://repo.saltstack.com/apt/debian/9/amd64/latest stretch main" > /etc/apt/sources.list.d/saltstack.list
    apt install salt-minion git python-pygit2 -y
    salt-call --local state.apply --state-output=mixed pillar='{"bootstrap": True}'
    ;;
  *)
    echo -n "unsupported or failed matching distro"
    ;;
esac
